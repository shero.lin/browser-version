import * as dotenv from 'dotenv';
import * as path from 'path';

dotenv.config();

const FILE = process.env.FILE || '/versions.txt';
const FILE_PATH = path.join(__dirname, FILE);
const WEBHOOK_URL = process.env.WEBHOOK_URL|| 'https://hooks.glip.com/webhook/e1d24f33-0305-4697-b17a-329ea8d4866a';
const WIKI_URL = process.env.WIKI_URL||''

export {
    FILE_PATH,
    WEBHOOK_URL,
    WIKI_URL
}