import axios from 'axios';
import Parser from './parser'
import { WIKI_URL } from '../config';

export default class Versions {

    static async versions(oldVersions: Array<any>) {
        const { data } = await axios.get(WIKI_URL);
        const updateVersion = Parser.parser(data, oldVersions);
        return updateVersion;
    }

}



