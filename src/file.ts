import * as fs from 'fs';

export default class File {
    static Write(path: string, data: any) {
        try {
            fs.writeFileSync(path, data);
        } catch (err) {
            console.error(err);
        }
    }

    static Read(path: string) {
        this.create(path);
        let str: any;
        try {
            str = fs.readFileSync(path, { encoding: 'utf-8' })
        } catch (err) {
            console.error(err);
        }
        return str;
    }
    static create(path: string) {
        let paths = path.split('/');
        for (let i = 2; i < paths.length + 1; i++) {
            let newPath = paths.slice(0, i).join('/');
            if (!fs.existsSync(newPath)) {
                if (i === paths.length) {
                    try {
                        fs.openSync(newPath, 'w');
                    } catch (err) {
                        console.error(err);
                    }
                } else {
                    try {
                        fs.mkdirSync(newPath);
                    } catch (err) {
                        console.error(err);
                    }
                }
            }
        }
    }
}