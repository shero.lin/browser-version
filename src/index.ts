import Versions from "./versions";
import File from './file'
import { FILE_PATH } from "../config";
import sendPost from "./post";

async function index() {
    const fileContent = File.Read(FILE_PATH);
    let oldVersions: Array<any> = [];
    if (fileContent) {
        oldVersions = JSON.parse(fileContent);
    }
    const updateVersion = await Versions.versions(oldVersions);
    sendPost(updateVersion);
}
index();
