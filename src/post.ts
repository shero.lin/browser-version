import axios from 'axios';
import { WEBHOOK_URL } from '../config';
import * as moment from 'moment';

export default function sendPost(updateVersions: Array<any>) {
    const icons = { "Google Chrome": "https://i.loli.net/2020/09/04/C7SlDobLFABmRsh.jpg", "Firefox": "https://i.loli.net/2020/09/04/6hCN9bSXOMRkPVK.jpg", "Microsoft Edge": "https://i.loli.net/2020/09/04/uq7GbPAdHWCnBgM.jpg", "Safari (web browser)": "https://i.loli.net/2020/09/04/RGJlXgUOw1iEfdb.jpg" }
    let text: string = '';
    updateVersions.forEach(updateVersion => {
        text = '**Update**  :\n*browser* : __' + updateVersion.browser + '__\n*os* : ' + updateVersion.os + '\n*version* : ' + updateVersion.version + '\n*updateTime*: ' + updateVersion.updateTime;
        const data = {
            "icon": icons[updateVersion.browser],
            "body": text
        }
        const headers = { 'Content-Type': 'application/json' }
        axios.post(WEBHOOK_URL, data, { headers }).then(res => console.log(res)).catch(err => console.log(err));
    })
}