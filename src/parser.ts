import { FILE_PATH } from "../config";
import File from './file';
const cheerio = require("cheerio");

export default class Parser {
    static parser(html: any, oldVersions: Array<any>) {
        const browsers = [
            {
                title: 'Google Chrome',
                osList: [{
                    name: 'Windows',
                    reg: /Windows/
                },
                {
                    name: 'macOS',
                    reg: /mac/
                }]
            },
            {
                title: 'Safari (web browser)',
                osList: [{
                    name: 'macOS',
                    reg: /mac/
                }]
            },
            {
                title: 'Firefox',
                osList: [{
                    name: 'Windows',
                    reg: /Standard/
                },
                {
                    name: 'macOS',
                    reg: /Standard/
                }]
            },
            {
                title: 'Microsoft Edge',
                osList: [{
                    name: 'Windows',
                    reg: /Windows/
                },
                {
                    name: 'macOS',
                    reg: /mac/
                }]
            },
        ]
        let versions = {
            'Google Chrome': {
                'Windows': '',
                'macOS': ''
            },
            'Safari (web browser)': {
                'macOS': ''
            },
            'Firefox': {
                'Windows': '',
                'macOS': ''
            },
            'Microsoft Edge': {
                'Windows': '',
                'macOS': ''
            }
        }

        let updateVersion: Array<any> = [];
        const $ = cheerio.load(html);
        browsers.forEach(
            browser => {
                const versionTable = $('table').eq(0).find('a[title="' + browser.title + '"]').parents('tr').find('table');
                let browserVersion: string, updateTime: string;
                versionTable.find('tr').each((i, ele) => {
                    browser.osList.forEach(
                        os => {
                            if (os.reg.test($(ele).text())) {
                                browserVersion = $(ele).find('td').text().split('/')[0].trim();
                                const str: string = $(ele).find('td').text().split('/')[1];
                                const regArr = str.match(/\((\d{4}-\d{2}-\d{2})\)/);
                                updateTime = regArr == null ? 'None' : regArr[1];
                                versions[browser.title][os.name] = browserVersion;
                                if (oldVersions[browser.title] == undefined || oldVersions[browser.title][os.name] == undefined || oldVersions[browser.title][os.name] != browserVersion) {
                                    updateVersion.push({
                                        "browser": browser.title,
                                        "os": os.name,
                                        "version": browserVersion,
                                        "updateTime": updateTime
                                    })
                                }

                            }
                        }
                    )
                })
            }
        )
        const data = JSON.stringify(versions);
        File.Write(FILE_PATH, data);
        return updateVersion;
    }

}